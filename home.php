<!-- Header Lead -->
<div class="jumbotron">
  <div class="container header-section">
    <div class="row">
      <div class="col-sm-4">
        <h1 style="color: white"><?= get_bloginfo('name') ?></h1>
        <p><?= get_bloginfo('description') ?></p>
        <br>
        <?= get_search_form() ?>
      </div>
    </div>
  </div>
</div>

<div class="container-fluid" style="background: #1f427e;">
  <?php include_once(__DIR__.'/inc/section/social.php'); ?>
</div>
<!-- End Lead header -->

<!-- Project Section -->
<div class="container-fluid" style="background-color: #f9f9f9;">
  <?php include_once(__DIR__.'/inc/section/project.php'); ?>
</div>
<!--  End Project -->

<!-- Service Section -->
<div class="container-fluid" style="background-color: #fff;">
  <?php include_once(__DIR__.'/inc/section/service.php'); ?>
</div>
<!-- End Service Section -->

<!--  Our Tech -->
<div class="container-fluid bg-primary" style="background-color: #1f427e">
  <?php include_once(__DIR__.'/inc/section/tech.php'); ?>
</div>
<!-- End Our Tech -->

<!-- Team -->
<div class="container-fluid" style="padding: 10px; background-color: #f3f3f3">
  <?php include_once(__DIR__.'/inc/section/team.php'); ?>
</div>
<!-- End Team -->

<!-- Contact -->
<div class="container-fluid">
  <?php include_once(__DIR__.'/inc/section/contact.php'); ?>
</div>
<!-- End Contact -->