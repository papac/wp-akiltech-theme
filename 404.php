<?= get_header() ?>
<div class="container-fluid" style="background: #1f427e; color: #fff;">
  <div class="container header-section" style="padding: 100px;">
    <div class="row">
      <div class="col-sm-6">
        <h1 style="color: white">404</h1>
        <p>Désolé cette page n'existe pas.<br />Akil Technologies, vous offre un service de qualité.</p>
      </div>
    </div>
  </div>
</div>
<?= get_footer() ?>