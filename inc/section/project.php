<div class="container project-section bg-white shadow" style="position: relative; top: -50px; padding: 40px;">
  <div class="row">
    <div class="col-sm-3">
      <h2 style="font-weight: bolder">Nos Offre</h2>
    </div>
    <div class="col-sm-9">
      <p>
        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Adipisci alias assumenda at beatae consequatur delectus earum error esse exercitationem expedita illum magni modi natus, neque nostrum rem, saepe sequi voluptate?
      </p>
    </div>
  </div>
  <br>
  <div class="row">
    <div class="col-sm-4">
      <div class="thumbnail">
        <img src="<?= get_bloginfo('template_directory')?>/assets/svg/risks.svg" style="width: 50px">
        <div class="caption">
          <h3>J'ai besoin d'un développeur</h3>
          <p>Eius populus ab incunabulis primis ad usque pueritiae tempus extremum, quod annis circumcluditur fere trecentis, circummurana pertulit bella, deinde aetatem ingressus adultam post multiplices bellorum aerumnas Alpes transcendit et fretum, in iuvenem erectus et virum ex omni plaga quam orbis ambit inmensus,</p>
          <p><a href="#" class="btn btn-primary" role="button">Commencer</a></p>
        </div>
      </div>
    </div>
    <div class="col-sm-4">
      <div class="thumbnail">
        <img src="<?= get_bloginfo('template_directory')?>/assets/svg/risks.svg"  style="width: 50px">
        <div class="caption">
          <h3>Je veux soumettre mon projet</h3>
          <p>Eius populus ab incunabulis primis ad usque pueritiae tempus extremum, quod annis circumcluditur fere trecentis, circummurana pertulit bella, deinde aetatem ingressus adultam post multiplices bellorum aerumnas Alpes transcendit et fretum, in iuvenem erectus et virum ex omni plaga quam orbis ambit inmensus,</p>
          <p><a href="#" class="btn btn-primary" role="button">Commencer</a></p>
        </div>
      </div>
    </div>
    <div class="col-sm-4">
      <div class="thumbnail">
        <img src="<?= get_bloginfo('template_directory')?>/assets/svg/risks.svg" style="width: 50px">
        <div class="caption">
          <h3>Je veux soumettre mon projet</h3>
          <p>Eius populus ab incunabulis primis ad usque pueritiae tempus extremum, quod annis circumcluditur fere trecentis, circummurana pertulit bella, deinde aetatem ingressus adultam post multiplices bellorum aerumnas Alpes transcendit et fretum, in iuvenem erectus et virum ex omni plaga quam orbis ambit inmensus,</p>
          <p><a href="#" class="btn btn-primary" role="button">Commencer</a></p>
        </div>
      </div>
    </div>
  </div>
</div>