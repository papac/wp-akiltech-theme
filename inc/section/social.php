<div class="container" style="color: #fff; margin-bottom: 80px;">
  <div class="row">
    <div class="col-sm-6 col-xs-12">
      <ul class="nav nav-pills pull-left">
        <li>
          <a href="#">
            <img src="<?= get_bloginfo('template_directory')?>/assets/svg/facebook.svg" style="width: 20px;">
          </a>
        </li>
        <li>
          <a href="#">
            <img src="<?= get_bloginfo('template_directory')?>/assets/svg/twitter.svg" style="width: 20px;">
          </a>
        </li>
        <li>
          <a href="#">
            <img src="<?= get_bloginfo('template_directory')?>/assets/svg/youtube.svg" style="width: 20px;">
          </a>
        </li>
        <li>
          <a href="#">
            <img src="<?= get_bloginfo('template_directory')?>/assets/svg/instagram.svg" style="width: 20px;">
          </a>
        </li>
      </ul>
    </div>
    <div class="col-sm-6 col-xs-12">
      <ul class="nav nav-pills pull-right">
        <li>
          <a href="">
            <img src="<?= get_bloginfo('template_directory')?>/assets/svg/mobile-phone.svg" style="width: 20px;">
          </a>
        </li>
      </ul>
    </div>
  </div>
</div>