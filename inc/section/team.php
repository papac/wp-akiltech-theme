<div class="container" style="padding: 50px;">
    <div class="row">
      <div class="col-sm-4 col-xs-12">
        <h2>Akiler</h2>
        <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Iusto fuga consectetur, exercitationem voluptates debitis aliquid voluptate rerum, recusandae eveniet est earum velit magni praesentium ea, dolorem adipisci nulla perferendis minus. Incidunt ea eligendi temporibus sit! Numquam aut sunt iure maiores iusto libero repellendus cumque aliquid rerum laboriosam officiis ipsum quidem, eveniet commodi in. Necessitatibus ex at, tenetur aspernatur aliquam saepe.</p>
        <br>
        <a class="btn btn-primary" href="/akilers">Voir l'équipe</a>
      </div>
      <div class="col-sm-8 col-xs-12">
        <h2>&nbsp;</h2>
        <div class="row">
          <?php foreach(range(0, 3) as $a): ?>
          <div class="col-sm-6 col-xs-12">
            <div class="media shadow-sm bg-white" style="padding: 5px; margin: 5px;">
              <div class="media-left">
                <a href="#">
                  <img class="media-object img-circle" src="https://avatars0.githubusercontent.com/u/582061?s=400&v=4" style="width: 80px">
                </a>
              </div>
              <div class="media-body" style="position: relative; bottom: -15px;">
                <h4 class="media-heading">Abou KONE</h4>
                <p>CTO Akil Technologies</p>
              </div>
            </div>
          </div>
          <?php endforeach; ?>
        </div>
      </div>
    </div>
  </div>