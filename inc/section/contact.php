<div class="container contact-section" style="padding-bottom: 5px; padding-top: 5px;">
    <h2 class="text-center" style="margin-bottom: 50px;">Contactez Nous</h2>
    <div class="row">
      <div class="col-sm-4">
        <div class="row">
          <div class="col-sm-12">
            <p>123, Rev Avenue, Yopougon</p>
            <Br>
            <ul class="nav" style="padding: 10px">
              <li><img src="<?= get_bloginfo('template_directory')?>/assets/svg/phone.svg" style="width: 20px;"> +22 225 222 222</li>
              <li><img src="<?= get_bloginfo('template_directory')?>/assets/svg/call.svg" style="width: 20px;"> +22 225 222 222</li>
            </ul>
          </div>
        </div>

        <div class="row">
          <div class="col-sm-12">
            <ul class="nav nav-pills"  style="padding: 10px">
              <li>
                <a href="">
                  <img src="<?= get_bloginfo('template_directory')?>/assets/svg/facebook-gray.svg" style="width: 20px;">
                </a>
              </li>
              <li>
                <a href="#">
                  <img src="<?= get_bloginfo('template_directory')?>/assets/svg/twitter-gray.svg" style="width: 20px;">
                </a>
              </li>
              <li>
                <a href="#">
                  <img src="<?= get_bloginfo('template_directory')?>/assets/svg/youtube-gray.svg" style="width: 20px;">
                </a>
              </li>
              <li>
                <a href="#">
                  <img src="<?= get_bloginfo('template_directory')?>/assets/svg/instagram-gray.svg" style="width: 20px;">
                </a>
              </li>
            </ul>
          </div>
        </div>
      </div>

      <div class="col-sm-offset-2 col-sm-6 col-xs-12">
        <p>123, Rev Avenue, yopougon</p>
        <form class="form-contact">
          <div class="form-group">
            <input class="form-control" type="text" name="name">
          </div>
          <div class="form-group">
            <input class="form-control" type="text" name="email">
          </div>
          <div class="form-group">
            <textarea name="message" id="message" class="form-control" style="resize: none"></textarea>
          </div>
          <div class="form-group">
            <button class="btn btn-primary">Envoyer</button>
          </div>
        </form>
      </div>
    </div>
  </div>