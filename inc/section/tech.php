<div class="container tech-section" style="padding: 100px; padding-top: 20px;">
    <div class="row">
      <div class="col-sm-12 text-center">
        <h2 style="color: #fff">Tech</h2>
      </div>
    </div>
    <br>
    <div class="row">
      <div class="col-sm-12 tech-item">
        <ul class="nav nav-pills nav-justified">
          <li>
            <h3 class="nav-header">Project Manager</h3>
            <ul class="nav text-left">
              <li>Atlassian</li>
            </ul>
          </li>
          <li>
            <h3 class="nav-header">Backend</h3>
            <ul class="nav text-left">
              <li>Node JS</li>
              <li>Symfony</li>
              <li>Laravel</li>
              <li>Java</li>
            </ul>
          </li>
          <li>
            <h3 class="nav-header">Frontend</h3>
            <ul class="nav text-left">
              <li>Reactjs</li>
              <li>Angular</li>
              <li>Angularjs</li>
              <li>Javascript</li>
              <li>Sveltjs</li>
            </ul>
          </li>
          <li>
            <h3 class="nav-header">Devops</h3>
            <ul class="nav text-left">
              <li>Docker</li>
              <li>Git</li>
              <li>Ansible</li>
              <li>Jenkins</li>
              <li>ELK</li>
            </ul>
          </li>
          <li>
            <h3 class="nav-header">Mobile</h3>
            <ul class="nav text-left">
              <li>React Native</li>
              <li>NativeScript</li>
              <li>Ionic</li>
              <li>Android</li>
              <li>Java</li>
            </ul>
          </li>
        </ul>
      </div>
    </div>
  </div>