<nav class="navbar navbar-default navbar-static-top">
  <div class="container">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" href="<?= get_bloginfo('wpurl') ?>">
        <img src="<?= get_bloginfo('template_directory')?>/assets/img/logo.png" alt="Akil Technologies">
      </a>
    </div>

    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
      <form class="navbar-form navbar-right">
        <button class="btn btn-primary" type="submit">Contactez nous</button>
      </form>

      <ul class="nav navbar-nav navbar-right">
        <li class="nav-item <?= is_page('accueil') ? 'active' : '' ?>">
          <a class="nav-link" href="<?= get_bloginfo('wpurl') ?>">Accueil <span class="sr-only">(current)</span></a>
        </li>
        <li class="nav-item <?= is_page('nos-offres') ? 'active' : '' ?>">
          <a class="nav-link" href="/nos-offres">Nos offres</a>
        </li>
        <li class="nav-item <?= is_page('techno') ? 'active' : '' ?>">
          <a class="nav-link" href="/techno">Tech</a>
        </li>
        <li class="nav-item <?= is_page('akilers') ? 'active' : '' ?>">
          <a class="nav-link" href="/akiler">Akilers</a>
        </li>
        <li class="nav-item <?= is_page('jobs') ? 'active' : '' ?>">
          <a class="nav-link" href="/jobs">Jobs</a>
        </li>
      </ul>
    </div>
  </div>
</nav>