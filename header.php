<!doctype html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
  <link rel="stylesheet" href="<?php echo get_bloginfo( 'template_directory' );?>/assets/css/app.css">
  <link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">
  <title><?= the_title() ?? "404" ?> | <?= ucfirst(get_bloginfo('name')) ?></title>
  <?php wp_head();?>
</head>
<body>

<?php include(__DIR__.'/inc/navbar.php'); ?>
