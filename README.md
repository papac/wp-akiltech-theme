# Akil Technologies

The wordpress theme for Akil Technologies Website.

# Installation

For install this template, clone this repository in your wordpress `wp-content/themes` folder.

```bash
git clone git@gitlab.com/akiltech/wp-website-theme.git
```

After your downloading. You must compile the application assets:

```bash
cd wp-website-theme
npm install
npm run dev
```

# Add the necessery page

For end the installation create the wordpress page

| Titre       | Description | permalink   |
| ----------- | ----------- | ----------- |
| Accueil     | Home page   | /           |
| Nos Offres  | Our Offre        | /nos-offres |
| Akilers     | The akil team        | /akilers    |
| Techno      | Thechnologie using by Akil       | /techno     |
| Jobs      | Jobs information       | /jobs     |


# Settings

In the settings.

1. You must define the permalink in `/%postname%/`
2. Define the default page to `Accueil`