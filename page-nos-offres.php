<?php get_header(); ?>
<?php the_post(); ?>

<div class="container-fluid" style="background-color: #f3f3f3; padding: 10px;">
  <div class="container">
    <div class="row">
      <div class="col-sm-12">
        <h1 class="text-muted text-center" style="font-size: 14px; font-weight: bold">
          <?= the_content() ?>
        </h1>
      </div>
    </div>
  </div>
</div>
<div class="container-fluid" style="background-color: #fff; padding: 40px;">
  <div class="container">
    <div class="row">
      <div class="col-sm-4">
        <h4 class="text-muted">Etape 1/5</h4>
        <div class="progress step">
          <div class="one"></div>
          <div class="two"></div>
          <div class="three"></div>
          <div class="four"></div>
          <div class="progress-bar" style="width: 50%;"></div>
        </div>
        <h3 style="width: 200px;" class="text-muted">Parlez-nous de votre projet</h3>
        <p>Eodem tempore Serenianus ex duce, cuius ignavia populatam in Phoenice Celsen ante rettulimus, pulsatae</p>
      </div>
      <div class="col-sm-8">
        <div class="step-1">
          <?php include_once(__DIR__.'/inc/step/1.php') ?>
        </div>
      </div>
    </div>
  </div>
</div>
<?php get_footer(); ?>